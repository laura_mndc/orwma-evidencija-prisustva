package hr.lmandic.evidencijaprisustva;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;


import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity  implements CustomClickListener {

    private RecyclerView recycler;
    private RecyclerAdapter adapter;
    private EditText etNewName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupRecycler();
        setupRecyclerData();
        etNewName=(EditText) findViewById(R.id.etNewName);

    }

    private void setupRecyclerData() {
        List<String> data=new ArrayList<>();
        data.add("Ana");
        data.add("Marko");
        data.add("Pero");
        adapter.addData(data);
    }

    private void setupRecycler() {
        recycler=findViewById(R.id.recyclerView);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        adapter=new RecyclerAdapter(this);
        recycler.setAdapter(adapter);

    }

    public void addCell(View view){
        String newName=etNewName.getText().toString();
        etNewName.getText().clear();
        adapter.addNewCell(newName, adapter.getItemCount());
    }


    @Override
    public void onClick(int index) {
        adapter.removeCell(index);
    }
}
