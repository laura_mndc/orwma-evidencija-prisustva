package hr.lmandic.evidencijaprisustva;

public interface CustomClickListener {
    void onClick(int position);
}
